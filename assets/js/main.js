var $window             = $(window);
$window.on("load", function () {
    "use strict";
    $(".preloader").delay(400).fadeOut(600, function () {
        $(this).remove();
    });
});
$(document).ready(function(){
    $(".owl-carousel").owlCarousel({
        margin:15,
        loop:true,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:4
            }
        }
    });
    if($('.gallery-page .item a').length != 0){
        $('.gallery-page .item a').simpleLightbox();
    }
});